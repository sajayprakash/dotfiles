# Sajay's Dotfiles
This is a repo of all the config files for my GNU/Linux system.

# Window Manager Configs
- [AwesomeWM](https://gitlab.com/sajayprakash/dotfiles/-/tree/main/config/awesome)
- [i3](https://gitlab.com/sajayprakash/dotfiles/-/tree/main/config/i3)

# Other Configs
- [Dmenu](https://gitlab.com/sajayprakash/dotfiles/-/tree/main/config/dmenu)
- [i3status](https://gitlab.com/sajayprakash/dotfiles/-/tree/main/config/i3status)
- [Alacritty](https://gitlab.com/sajayprakash/dotfiles/-/tree/main/config/alacritty)
- [Fish](https://gitlab.com/sajayprakash/dotfiles/-/tree/main/config/fish)
- [picom](https://gitlab.com/sajayprakash/dotfiles/-/blob/main/config/picom.conf)
- [Starship](https://gitlab.com/sajayprakash/dotfiles/-/blob/main/config/starship.toml)

I start my desktop using ``startx`` and using [my xinitrc file](https://gitlab.com/sajayprakash/dotfiles/-/blob/main/config/x11/xinitrc) 

# My Wallpaper
![wallpaper](https://gitlab.com/sajayprakash/dotfiles/-/raw/main/config/wall.png)

