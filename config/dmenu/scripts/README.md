# dmenu

shell scripts for dmenu

| folder name | comment              |
| :---------- | :------------------- |
| data        | data for the scripts |

| name                     | comment                                                                   |
| :----------------------- | :------------------------------------------------------------------------ |
| dmenu_bookmarks.sh       | search/sync bookmarks from firefox and open them in browser               |
| dmenu_calc.sh            | calculate with bc                                                         |
| dmenu_display.sh         | manage displays with xrandr                                               |
| dmenu_iwd.sh             | connect to wlan with iwd                                                  |
| dmenu_man.sh             | search for man pages                                                      |
| dmenu_pass.sh            | use username or password from gpg password store                          |
| dmenu_screenshot.sh      | take screenshot from desktop, window or selection                         |
| dmenu_shortcuts.sh       | search for defined shortcuts from a file                                  |
| dmenu_ssh.sh             | connect to hosts over ssh                                                 |
| dmenu_unicode_symbols.sh | search and copy unicode symbols                                           |
| dmenu_virtualbox.sh      | start a virtual machine without virtualbox frontend                       |
| dmenu_windows            | window switcher                                                           |
| dmenu_youtube.sh         | search youtube with yt-dlp and play video/audio with mpv or download them |

related projects:

- [i3](https://github.com/mrdotx/i3)
- [link-handler](https://github.com/mrdotx/link-handler)
- [shell](https://github.com/mrdotx/shell)
